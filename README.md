# weedingsim
A repo for my Weeding Simulator game. 

New updates won't come very often as I do a lot of my coding offline.

Join the Discord server: https://discord.gg/Demm836
# Initial Release

This game is still a prototype with not a lot of playability but I wanted to put it out there so I could get feedback on what people think of it

# How to Run

Just clone the repo or download and unzip it and go to the folder in your terminal and enter the following

```
pip3 install -r requirements.txt
chmod +x main.py game.py
```
and to run it:
`./main.py`

